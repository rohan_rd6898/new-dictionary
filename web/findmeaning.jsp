<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="jquery-3.5.0.min.js"></script>
        <script src="findmeaning.js"></script>
    </head>
    <body>
        <h1>  ENGLISH DICTIONARY!!</h1>
        <form action="findmeaning.jsp" method="POST">
        Enter the Word :
        <input type="text" name="wor" id="word">
        <input type="button" value = "Search" onclick="loaddata()">
        <br /><br />
        <div id ="dbdata"></div>
    </body>
</html>
