package CAMPS.findmeaning;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import CAMPS.Connect.DBConnect;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class findMeaning extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DBConnect db = new DBConnect();
        String data="";
        try (PrintWriter out = response.getWriter()) {
            String var_wo;
            var_wo = request.getParameter("wor");
            
            /* TODO output your page here. You may use following sample code. */
            if (request.getParameter("option")==null){
            out.print("no option is specified");
            }else if(request.getParameter("option").equalsIgnoreCase("display")) {
            db.getConnection();
            db.read("select word, word_type, meaning from dictionary.english_dictionary where word = '"+var_wo+"'");
            if(db.rs.next())
            {
                data+="<table border = '1'>";
                data+="<thead>";
                data+="<tr>";
                data+="<th>"+"Word"+"</th>"+"<th>"+"Word Type"+"</th>"+"<th>"+"Meaning"+"</th>";
                data+="</tr>";
                data+="</thead>";
            
            
            do
            {
                data+="<tr>";
                data+="<td>"+db.rs.getString("word")+"</td>"+"<td>"+db.rs.getString("word_type")+"</td>"+"<td>"+db.rs.getString("meaning")+"</td>";
                data+="</tr>";
            }while(db.rs.next());
            data+="</table>";
            data+="<br /><br />";
            }
            else
            {
                data+="OOPS !!! Word not found!!!<br /><br />";
            }
            
            db.read("select word, word_type, meaning from dictionary.english_dictionary where word like '%"+var_wo+"%' except select word, word_type, meaning from dictionary.english_dictionary where word = '"+var_wo+"'");
            if(db.rs.next())
            {
                data+="Words containing ";
                data+="<b><i>'"+var_wo+"'</i></b>"+"<br />";
                data+="<table border = '1'>";
                data+="<thead>";
                data+="<tr>";
                data+="<th>"+"Word"+"</th>"+"<th>"+"Word Type"+"</th>"+"<th>"+"Meaning"+"</th>";
                data+="</tr>";
                data+="</thead>";
            
            
            do
            {
                data+="<tr>";
                data+="<td>"+db.rs.getString("word")+"</td>"+"<td>"+db.rs.getString("word_type")+"</td>"+"<td>"+db.rs.getString("meaning")+"</td>";
                data+="</tr>";
            }while(db.rs.next());
            data+="</table>";
            }
            out.print(data);
            db.closeConnection();
            }
            else
                out.print("option undefined");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(findMeaning.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(findMeaning.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(findMeaning.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(findMeaning.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
